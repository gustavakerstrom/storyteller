export const highlightTints = {
  yellow: "rgba(255, 255, 0, 0.3)",
  red: "rgba(255, 0, 0, 0.3)",
  green: "rgba(0, 255, 0, 0.3)",
  blue: "rgba(0, 0, 255, 0.3)",
  magenta: "rgba(255, 0, 255, 0.3)",
}
